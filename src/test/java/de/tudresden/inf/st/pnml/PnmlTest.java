package de.tudresden.inf.st.pnml;

import de.tudresden.inf.st.pnml.jastadd.model.Marking;
import de.tudresden.inf.st.pnml.jastadd.model.PetriNet;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Optional;
import java.util.Random;

class PnmlTest {

  private static final Logger logger = LoggerFactory.getLogger(PnmlTest.class);

  /**
   * This is a copy of the test from the marking dependency to see if all features of it still work here
   * @param fileName file name of test PNML file
   */
  @ParameterizedTest
  @ValueSource(strings = {"src/test/resources/minimal.pnml", "src/test/resources/philo.pnml", "src/test/resources/haddadin_automaton_flat.pnml"})
  void testExecution(String fileName) {
    List<PetriNet> petriNets = PnmlParser.parsePnml(fileName);

    for (PetriNet petriNet : petriNets) {
      Marking originalMarking = petriNet.initialMarking();
      Marking currentMarking = originalMarking;
      logger.info("initial marking:\n{}", originalMarking.print());

      // always use seed 0, it is the best seed!
      Random random1 = new Random(0);
      Random random2 = new Random(0);

      for (int i = 1; i <= 5 && !originalMarking.isDead() && !currentMarking.isDead(); i++) {

        Optional<Marking> optionalCurrentMarking = currentMarking.fire(random1);
        if (optionalCurrentMarking.isPresent()) {
          currentMarking = optionalCurrentMarking.get();
        } else {
          throw new RuntimeException("Unable to get successor marking, even though the marking is not dead!");
        }
        originalMarking.fireInPlace(random2);
        logger.info("Original marking after {} firings: {}", i, originalMarking.print());
        logger.info("Current marking after {} firings: {}", i, currentMarking.print());
      }

      logger.info("DOT file:\n\n{}", petriNet.toDot());
    }
  }

}
